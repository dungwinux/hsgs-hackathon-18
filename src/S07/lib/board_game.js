"use strict";
Object.defineProperty(exports, "__esModule", {value: true});


debugger
const BoardGame = {
    default(props = {size: 9}) {
        const field = [];
        const playField = [];
        const opt = ["+", "-", "*"];
        for (let i = 0; i < props.size; i++) {
            field.push([]);
            playField.push([]);
            for (let j = 0; j < props.size; j++) {
                field[i].push(-1);
                playField[i].push(-1);
            }
        }
        for (let i = 0; i < props.size - 2; i++) {
            for (let j = 0; j < props.size - 2; j++) {
                if (field[i][j] >= 0 || (i % 2 === 1 && j % 2 === 1)) {
                    continue;
                }
                else if (i % 2 === 0 && j % 2 === 0) {
                    field[i][j] = Math.floor(Math.random() * 10) + 1;
                }
                else if (i % 2 === 1 || j % 2 === 1) {
                    field[i][j] = opt[Math.floor(Math.random() * 3)];
                }
                playField[i][j] = -2;
            }
        }


        for (let i = 0; i < props.size - 2; i += 2) {
            field[i][props.size - 2] = "=";
            playField[i][props.size - 2] = "=";
            let ans = "";
            for (let j = props.size - 3; j >= 0; j -= 2) {
                ans += (field[i][j] + " " + field[i][j - 1]);
            }
            ans = eval(ans);
            field[i][props.size - 1] = ans;
            playField[i][props.size - 1] = ans;
        }


        for (let i = 0; i < props.size - 2; i += 2) {
            field[props.size - 2][i] = "=";
            playField[props.size - 2][i] = "=";
            let ans = 0;
            /*
            for (let j = props.size - 3; j >= 0; j -= 2) {
                ans += calculateBack(field[j - 1][i], ans, field[j][i]);
            }
            */
            field[props.size - 1][i] = ans;
            playField[props.size - 1][i] = ans;
        }

        return {field, playField};
    },

    actions: {
        async move(state, {x, y, val}) {
            if (x > props.size || y > props.size) {
                throw new Error("Bạn không thể điến số vào đây");
            }
            x--;
            y--;

            if (x % 2 === 1 && y % 2 === 1) {
                throw new Error("Không thể điền vào ô đen");
            }

            if (x % 2 === 0 && y % 2 === 0 && (val === "+" || val === "-" || val === "*")) {
                throw new Error("Bạn không thể điền dấu vào đây");
            }

            if (x % 2 === 1 || y % 2 === 1 && Number.isInteger(val)) {
                throw new Error("Bạn không thể điến số vào đây");
            }
            const newField = playField.slice();
            newField[y][x] = val;

            console.log(state);
            return {newField};
        }
    },
    isValid(state) {
        return true;
    },
    isEnding(state) {
        //TODO Changing Algorithm to fit the new refractor
        for (let i = 0; i < state.playField.length - 2; i += 2) {
            let ans = "";
            for (let j = state.playField[i].length - 2; j >= 0; j -= 2) {
                ans += (state.playField[i][j + 1] + " " + state.playField[i][j]);
            }
            ans = eval(ans);
            if (ans !== state.playField[i][state.playField[i].length - 1]) {
                return null;
            }
        }

        /*
                for (let i = 0; i < state.playField.length - 2; i += 2) {
                    let ans = 0;
                    for (let j = 0; j < state.playField[i].length - 2; j += 2) {
                        ans += calculateBack(state.playField[j + 1][i], ans, state.playField[j][i]);
                    }
                    if (ans !== state.playField[state.playField[i].length - 1][i]) {
                        return null;
                    }
                }

                return "won";
                */
    }
};
exports.default = BoardGame;